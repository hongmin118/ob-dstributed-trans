package com.alibaba.druid.util;

import com.alibaba.druid.support.logging.Log;
import com.alibaba.druid.support.logging.LogFactory;
import com.oceanbase.jdbc.OceanBaseConnection;
import com.oceanbase.jdbc.OceanBaseXaConnection;
import oracle.jdbc.driver.T4CXAConnection;
import oracle.jdbc.xa.client.OracleXAConnection;

import javax.sql.XAConnection;
import javax.transaction.xa.XAException;
import java.sql.Connection;

public class OBOracleUtils {
    private final static Log LOG = LogFactory.getLog(OracleUtils.class);

    public static XAConnection OBOracleXAConnection(Connection connection) throws XAException {
        String obOracleConnectionClassName = connection.getClass().getName();
        /*
        if ("oracle.jdbc.driver.T4CConnection".equals(oracleConnectionClassName)) {
            return new T4CXAConnection(oracleConnection);
        } else {
            return new OracleXAConnection(oracleConnection);
        }*/

        return new OceanBaseXaConnection((OceanBaseConnection) connection);

    }
}
