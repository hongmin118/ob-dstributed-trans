package com.cz.mhm;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ObDstrTransApplication {

    public static void main(String[] args) {
        SpringApplication.run(ObDstrTransApplication.class, args);
    }

}
