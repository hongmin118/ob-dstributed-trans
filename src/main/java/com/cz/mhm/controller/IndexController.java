package com.cz.mhm.controller;

import com.oceanbase.jdbc.OceanBaseConnection;
import com.oceanbase.jdbc.jdbc2.optional.JDBC4MysqlXAConnection;
import com.oceanbase.jdbc.jdbc2.optional.MysqlXid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Objects;


import oracle.jdbc.xa.OracleXid;
import oracle.jdbc.xa.OracleXAException;
import oracle.jdbc.pool.*;
import oracle.jdbc.xa.client.*;

import javax.sql.DataSource;
import javax.transaction.xa.*;

/**
 * @author mahone
 * @data 2023/8/9 18:59
 */
@RestController
@EnableAutoConfiguration
public class IndexController {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    @RequestMapping("/")
    @ResponseBody
    String index() throws Exception {
        return "hello ob dstributed-transactions";
    }


    @RequestMapping("/select")
    @ResponseBody
    String select() throws Exception {
        String result = "";
        try {
            DataSource dataSource = jdbcTemplate.getDataSource();
            Connection conn = null;
            if(dataSource != null) {
                conn = dataSource.getConnection();
                if(conn == null) {
                    return "ob conn is null";
                }
            }

            PreparedStatement pstmt = null;
            ResultSet rs = null;
            pstmt = conn.prepareStatement("select GOODS_NAME from MHM_GOODS");
            rs = pstmt.executeQuery();

            while (rs.next()) {
                result = rs.getString(1);
                System.out.println(result);
            }
            pstmt.close();

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return "oceanbase select: " + result;
    }



    @RequestMapping("/one")
    @ResponseBody
    String one() throws Exception {

        Connection conn = Objects.requireNonNull(jdbcTemplate.getDataSource()).getConnection();
        // conn.createStatement().execute(" insert into MHM_GOODS (ID, GOODS_NAME, GOODS_PRICE, GOODS_STOCK) values(1001, '按按aa', 100, 10)");

        JDBC4MysqlXAConnection mysqlXAConnection = new JDBC4MysqlXAConnection((OceanBaseConnection) conn);



        String gtridStr = "gtrid_test_wgs_ob_oracle_xa_one";
        String bqualStr = "bqual_test_wgs_ob_oracle_xa_one";
        Xid xid = new MysqlXid(gtridStr.getBytes(), bqualStr.getBytes(), 123);

        try {
            mysqlXAConnection.start(xid, XAResource.TMNOFLAGS);
            PreparedStatement pstmt = null;
            ResultSet rs = null;
            pstmt = conn.prepareStatement("select GOODS_NAME from MHM_GOODS");
            rs = pstmt.executeQuery();

            while (rs.next()) {
                System.out.println(rs.getInt(1));
            }
            pstmt.close();
            pstmt = conn.prepareStatement("insert into MHM_GOODS (ID, GOODS_NAME, GOODS_PRICE, GOODS_STOCK) values(?, ?, ?, ?)");
            pstmt.setInt(1, 1002);
            pstmt.setString(2, "报表bb");
            pstmt.setDouble(1, 200.22);
            pstmt.setInt(2, 222);
            pstmt.executeUpdate();
            mysqlXAConnection.end(xid, XAResource.TMSUCCESS);
            mysqlXAConnection.prepare(xid);
            mysqlXAConnection.commit(xid, false);
        } catch (Exception e) {
            e.printStackTrace();
            mysqlXAConnection.rollback(xid);
            throw e;
        }


        return "hello ob dstributed-transactions";
    }





}
